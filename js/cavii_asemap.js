/**
 * Created by huafan on 9/15/15.
 */
//
var app = angular.module('app', ['ngTouch', 'ngAnimate', 'ui.router', 'ui.grid', 'ui.grid.exporter', 'ui.grid.selection', 'ui.grid.grouping', 'ui.grid.resizeColumns', 'ui.bootstrap'])


//app.controller('loginController', ['$scope', '$state', function ($scope, $state) {
app.controller('asemapController', function ($scope, $state) {

    $scope.question = "What is the question? (Click the 'Configure' button on the Top-Bar)";

    $scope.attributes = [
        {
            name: 'Attribute 1'
        },
        {
            name: 'Attribute 2'
        },
        {
            name: 'Attribute 3'
        },
        {
            name: 'Attribute 4'
        },
        {
            name: 'Attribute 5'
        },
        {
            name: 'Attribute 6'
        }
    ];

    $scope.tradeoffPairs = [
        {
            attrLeft: 'Attribute 1',
            attrLeftDesc: 'Attribute 1',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 2',
            attrRightDesc: 'Attribute 2',
            rightImage: 'img/happy.png'
        },
        {
            attrLeft: 'Attribute 2',
            attrLeftDesc: 'Attribute 2',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 3',
            attrRightDesc: 'Attribute 3',
            rightImage: 'img/happy.png'
        },
        {
            attrLeft: 'Attribute 3',
            attrLeftDesc: 'Attribute 3',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 4',
            attrRightDesc: 'Attribute 4',
            rightImage: 'img/happy.png'
        },
        {
            attrLeft: 'Attribute 4',
            attrLeftDesc: 'Attribute 4',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 5',
            attrRightDesc: 'Attribute 5',
            rightImage: 'img/happy.png'
        },
        {
            attrLeft: 'Attribute 5',
            attrLeftDesc: 'Attribute 5',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 6',
            attrRightDesc: 'Attribute 6',
            rightImage: 'img/happy.png'
        },
        {
            attrLeft: 'Attribute 1',
            attrLeftDesc: 'Attribute 1',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 4',
            attrRightDesc: 'Attribute 4',
            rightImage: 'img/happy.png'
        },
        {
            attrLeft: 'Attribute 2',
            attrLeftDesc: 'Attribute 2',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 5',
            attrRightDesc: 'Attribute 5',
            rightImage: 'img/happy.png'
        },
        {
            attrLeft: 'Attribute 3',
            attrLeftDesc: 'Attribute 3',
            leftImage: 'img/happy.png',
            leftValue: 50,
            value: 5,
            rightValue: 50,
            attrRight: 'Attribute 6',
            attrRightDesc: 'Attribute 6',
            rightImage: 'img/happy.png'
        }
    ];

    $scope.$watch('attributes', function (newVal, oldVal) {
        for (var i = 0; i < $scope.attributes.length; i++) {
            //console.log(oldVal[i].value+' -> '+newVal[i].value);
            if (newVal[i].description != oldVal[i].description) {
                for (var j = 0; j < $scope.tradeoffPairs.length; j++) {
                    if ($scope.tradeoffPairs[j].attrLeft == newVal[i].name) {
                        $scope.tradeoffPairs[j].attrLeftDesc = newVal[i].description;
                    }
                    if ($scope.tradeoffPairs[j].attrRight == newVal[i].name) {
                        $scope.tradeoffPairs[j].attrRightDesc = newVal[i].description;
                    }
                }
                resetTradeOff();
                continue;
            } else if (newVal[i].imageURL != oldVal[i].imageURL) {
                for (var j = 0; j < $scope.tradeoffPairs.length; j++) {
                    if ($scope.tradeoffPairs[j].attrLeft == newVal[i].name) {
                        if (newVal[i].name == null || newVal[i].name == '')
                            $scope.tradeoffPairs[j].leftImage = 'img/happy.png';
                        else
                            $scope.tradeoffPairs[j].leftImage = newVal[i].imageURL;
                    }
                    if ($scope.tradeoffPairs[j].attrRight == newVal[i].name) {
                        if (newVal[i].name == null || newVal[i].name == '')
                            $scope.tradeoffPairs[j].rightImage = 'img/happy.png';
                        else
                            $scope.tradeoffPairs[j].rightImage = newVal[i].imageURL;
                    }
                }
                continue;
            }
        }
    }, true);

    $scope.$watch('tradeoffPairs', function (newVal, oldVal) {
        for (var i = 0; i < $scope.tradeoffPairs.length; i++) {
            //console.log(oldVal[i].value+' -> '+newVal[i].value);
            if (newVal[i].value != oldVal[i].value) {
                $scope.tradeoffPairs[i].leftValue = (11 - newVal[i].value) * 7;
                $scope.tradeoffPairs[i].rightValue = newVal[i].value * 7;
                continue;
            }
        }
    }, true);

    $scope.showConfig = false;
    $scope.showTradeoff = true;
    $('#configure').on('click', function () {
        $scope.showConfig = !$scope.showConfig;
        $scope.showTradeoff = !$scope.showTradeoff;
        $scope.$apply();
    });

    //create the spinner
    var spinneropts = {
        lines: 13 // The number of lines to draw
        , length: 5 // The length of each line
        , width: 3 // The line thickness
        , radius: 8 // The radius of the inner circle
        , scale: 1 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#fff' // #rgb or #rrggbb or array of colors
        , opacity: 0.75 // Opacity of the lines
        , rotate: 0 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1 // Rounds per second
        , trail: 60 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , zIndex: 2e9 // The z-index (defaults to 2000000000)
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '50%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
    };
    var target = document.getElementById('topbar');
    var spinner = new Spinner(spinneropts);

    function createResultsPopup() {
        $.jAlert({
            'title': 'Attribute Importance',
            'size':'xlg',
            'iframeHeight':false,
            'showAnimation':'lightSpeedIn',
            'hideAnimation': 'lightSpeedOut',
            'content': '<div class="container-fluid" style="padding-left: 5px; padding-right: 5px;"> ' +
            '<div class="row well" style="padding: 5px; margin: 5px;"> ' +
            '<div id="importance" style="text-align:center;margin: 0 auto"></div> ' +
            '</div> ' +
            '</div>',
            'theme': 'blue',
            'onOpen': function (alert) {
                createImportanceCol();
                alert.find('form').on('submit', function (e) {
                    e.preventDefault();
                });
            }
        });
    }

    function createImportanceCol() {
        var importancecolumn = $("#importance").highcharts();
        if (importancecolumn != null)
            loadImportanceCol();
        else {
            $('#importance').highcharts({
                chart: {
                    type: 'column',
                    events: {
                        load: function () {
                            loadImportanceCol();
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Lucida Grande'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '% Importance'
                    }
                },
                legend: {
                    enabled: true
                },

                tooltip: {
                    formatter: function () {
                        var points = this.points;
                        var pointsLength = points.length;
                        var tooltipMarkup = pointsLength ? '<span style="font-size: 10px">' + points[0].key + '</span><br/>' : '';
                        var index;
                        var y_value;

                        for (index = 0; index < pointsLength; index += 1) {
                            y_value = (points[index].y).toFixed(1);

                            tooltipMarkup += '<span style="color:' + points[index].series.color + '">\u25CF</span> ' + points[index].series.name + ': <b> ' + y_value + '%</b><br/>';
                        }

                        return tooltipMarkup;
                    },
                    shared: true
                }
            });
        }
    }

    var analysis_count = 1;
    function loadImportanceCol() {
        var importancecolumn = $("#importance").highcharts();

        for (var i = 0; i < $scope.importance.length; i++) {
            var series = $scope.importance[i];
            var chartdata = [];
            for (var j=0; j<series.importances.length; j++) {
                var datapoint = [series.importances[j].description, series.importances[j].importance];
                chartdata.push(datapoint);
            }
            importancecolumn.addSeries(
                {
                    name: series.name,
                    data: chartdata
                }
            );
        }
    };

    $scope.importance = [];
    function resetTradeOff() {
        $scope.importance.length = 0;
        for (var i=0; i<$scope.tradeoffPairs.length; i++) {
            $scope.tradeoffPairs[i].value = 5;
        }
    }
    $scope.resetClicked = function() {
        resetTradeOff();
    }

    $scope.resultsClicked = function() {
        createResultsPopup();
    };

    $scope.analyzeClicked = function() {

        spinner.spin(target);

        var payload = "attrs=c(";
        for (var i = 0; i < $scope.tradeoffPairs.length; i++) {
            payload += "'" + $scope.tradeoffPairs[i].attrLeft + "," + $scope.tradeoffPairs[i].attrRight + "'";
            if (i < $scope.tradeoffPairs.length - 1)
                payload += ",";
        }
        payload += ")&chips=c(";
        for (var i = 0; i < $scope.tradeoffPairs.length; i++) {
            payload += "'" + (11 - $scope.tradeoffPairs[i].value) + "," + $scope.tradeoffPairs[i].value + "'";
            if (i < $scope.tradeoffPairs.length - 1)
                payload += ",";
        }
        payload += ")";

        $.ajax({
            type: "POST",
            url: "http://54.152.66.250/ocpu/github/tridentasf/attributeImportance/R/getAttributeImportance/json",
            data: payload,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            //dataType: "json",
            success: function (response) {
                if (response != null) {
                    var series = [];
                    for (var i = 0; i < response.length; i++) {
                        var importance = {
                            name: response[i]._row.replace(".", " "),
                            importance: response[i].importance
                        };

                        for (var j = 0; j < $scope.attributes.length; j++) {
                            if ($scope.attributes[j].name == importance.name) {
                                if ($scope.attributes[j].description!=null)
                                    importance.description = $scope.attributes[j].description;
                                else
                                    importance.description = $scope.attributes[j].name;
                            }
                        }

                        series.push(importance);
                    }
                    series.sort(compare);

                    $scope.importance.push(
                        {
                            name: 'Analysis ' + (analysis_count++),
                            importances: series
                        }
                    )

                    createResultsPopup();

                    spinner.stop();
                } else {
                    spinner.stop();
                    alert('Computation server encountered error');
                }
            },
            error: function (errMsg) {
                spinner.stop();
                alert(JSON.stringify(errMsg));
            }
        });

        function compare(a, b) {
            if (a.importance > b.importance)
                return -1;
            if (a.importance < b.importance)
                return 1;
            return 0;
        }
    }

});